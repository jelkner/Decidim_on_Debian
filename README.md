# Decidim Installation on Debian GNU/Linux

Detailed installation notes for installing Decidim on Debian GNU/Linux.

## Files

* [install-decidim.sh](install-decidim.sh) - installation shell script
* [List of Debian Packages on Base Install](base_install_package_list.txt)
* [Install Decidim on Debian 12](DecidimOnDebian12.md)
